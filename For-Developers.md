# Notes For Developers

## Dependencies
Requires a compiler (e.g. gcc) that supports C++11.

## Compile
```
git clone git@gitlab.com:vgg/fii.git
cd fii
g++ -O2 -Isrc/ src/fii_util.cc src/fii.cc -o fii -lm -fopenmp -lpthread

./fii --self-test         # run tests (optional)
./fii --help              # show help
./fii /dataset/COCO2017/  # find identical images in COCO2017
```

## Check for Memory Leaks
```
valgrind --leak-check=full ./fii --self-test
```